<?php
namespace app\controller;

use app\BaseController;
use think\facade\View;

class Index extends BaseController
{
    public function index()
    {
        return View::fetch();
    }

    public function initData()
    {
        $dragWrap = input('post.dragWrap');
        $dragBtn = input('post.dragBtn');

        $slideAreaNum = 10; // 误差值，这个值可以改
        $randRot = rand(30, 270);

        $sucLenMin = (360 - $slideAreaNum - $randRot) * ($dragWrap - $dragBtn) / 360;
        $sucLenMax = (360 + $slideAreaNum - $randRot) * ($dragWrap - $dragBtn) / 360;

        session('captcha', [
            'min' => $sucLenMin,
            'max' => $sucLenMax
        ]);

        return json(['code' => 0, 'data' => [
            'randRot' => $randRot
        ], 'msg' => 'success']);
    }

    public function verify()
    {
        $disLf = input('post.disLf');
        $phone = input('post.phone');

        $verifyData = session('captcha');
        if ($verifyData['min'] <= $disLf && $verifyData['max'] >= $disLf) {

            // TODO 根据 $phone 发短信

            return json(['code' => 0, 'data' => [], 'msg' => '验证成功']);
        }

        return json(['code' => -1, 'data' => [], 'msg' => '验证失败']);
    }
}
